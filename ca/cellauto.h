/******************************************************************************
BSD 3-Clause License

Copyright (c) 2019, Valerio Orlandini
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/

#ifndef CELLAUTO_H_
#define CELLAUTO_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct
{
    int *states;
    unsigned int rows;
    unsigned int cols;
    int survive[9];
    int born[9];
} ca_matrix;

int init_matrix(ca_matrix *matrix, unsigned int rows, unsigned int cols);

int resize_matrix(ca_matrix *matrix, unsigned int rows, unsigned int cols);
int set_cell_state(ca_matrix *matrix, unsigned int row, unsigned int col, unsigned int state);
int set_rule(ca_matrix *matrix, int is_survive, unsigned int neighbors, int state);
int invert_cell_state(ca_matrix *matrix, unsigned int row, unsigned int col);
int get_cell_state(ca_matrix *matrix, unsigned int row, unsigned int col);
int get_alive_neighbors(ca_matrix *matrix, unsigned int row, unsigned int col);

void update_matrix(ca_matrix *matrix);

#endif // CELLAUTO_H_