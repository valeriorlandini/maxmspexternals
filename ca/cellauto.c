/******************************************************************************
BSD 3-Clause License

Copyright (c) 2019, Valerio Orlandini
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/

#include "cellauto.h"

int init_matrix(ca_matrix *matrix, unsigned int rows, unsigned int cols)
{
    if (matrix != NULL && (rows * cols) != 0)
    {
        matrix->states = (int *)malloc(rows * cols * sizeof(int));
        matrix->rows = rows;
        matrix->cols = cols;

        // Initialize Conway's rules
        for (unsigned int s = 0; s < 10; s++)
        {
            matrix->born[s] = 0;
            matrix->survive[s] = 0;
        }
        matrix->survive[2] = 1;
        matrix->survive[3] = 1;
        matrix->born[3] =  1;

        for (unsigned int v = 0; v < rows * cols; v++)
        {
            matrix->states[v] = 0;
        }

        return 0;
    }

    return -1;
}

int resize_matrix(ca_matrix *matrix, unsigned int rows, unsigned int cols)
{
    /*
    ca_matrix new_matrix;
    if (!init_matrix(&new_matrix, rows, cols))
    {
        int old_rows = rows;
        int old_cols = cols;

        if (rows > matrix->rows)
        {
            old_rows = matrix->rows;
        }

        if (cols > matrix->cols)
        {
            old_rows = matrix->cols;
        }


    }*/
    if (matrix != NULL && (rows * cols) != 0)
    {
        matrix->states = (int *)malloc(rows * cols * sizeof(int));
        matrix->rows = rows;
        matrix->cols = cols;

        for (unsigned int v = 0; v < rows * cols; v++)
        {
            matrix->states[v] = 0;
        }

        return 0;
    }

    return -1;
}

int set_cell_state(ca_matrix *matrix, unsigned int row, unsigned int col, unsigned int state)
{
    if (row < matrix->rows && col < matrix->cols)
    {
        if (state > 0)
        {
            matrix->states[(row * matrix->cols) + col] = 1;
        }
        else
        {
            matrix->states[(row * matrix->cols) + col] = 0;
        }

        return 0;
    }

    return -1;
}

int set_rule(ca_matrix *matrix, int is_survive, unsigned int neighbors, int state)
{
    if (neighbors < 9)
    {
        if (is_survive > 0)
        {
            matrix->survive[neighbors] = state;
        }
        else
        {
            matrix->born[neighbors] = state;
        }

        return 0;
    }

    return -1;
}

int invert_cell_state(ca_matrix *matrix, unsigned int row, unsigned int col)
{
    if (row < matrix->rows && col < matrix->cols)
    {
        if (matrix->states[(row * matrix->cols) + col] > 0)
        {
            matrix->states[(row * matrix->cols) + col] = 0;
        }
        else
        {
            matrix->states[(row * matrix->cols) + col] = 1;
        }

        return 0;
    }

    return -1;
}

int get_cell_state(ca_matrix *matrix, unsigned int row, unsigned int col)
{
    if (row < matrix->rows && col < matrix->cols)
    {
        return matrix->states[(row * matrix->cols) + col];
    }

    return -1;
}

int get_alive_neighbors(ca_matrix *matrix, unsigned int row, unsigned int col)
{
    int alive_neighbors = 0;
    int current_row, current_col;

    for (int nr = -1; nr < 2; nr++)
    {
        current_row = (int)row + nr;
        // Wrap matrix if necessary
        if (current_row < 0)
        {
            current_row = matrix->rows - 1;
        }
        if (current_row >= matrix->rows)
        {
            current_row = 0;
        }

        for (int nc = -1; nc < 2; nc++)
        {
            current_col = (int)col + nc;
            // Wrap matrix if necessary
            if (current_col < 0)
            {
                current_col = matrix->cols - 1;
            }
            if (current_col >= matrix->cols)
            {
                current_col = 0;
            }


            //printf("asking for state of cell %i,%i\n", current_row, current_col);
            if (get_cell_state(matrix, (unsigned int)current_row, (unsigned int)current_col) > 0)
            {
                ++alive_neighbors;
            }
        }
    }

    // Exclude interest cell from neighbor count
    if (get_cell_state(matrix, row, col) > 0)
    {
        --alive_neighbors;
    }

    return alive_neighbors;
}

void update_matrix(ca_matrix *matrix)
{
    ca_matrix new_matrix;
    init_matrix(&new_matrix, matrix->rows, matrix->cols);

    for (int r = 0; r < matrix->rows; r++)
    {
        for (int c = 0; c < matrix->cols; c++)
        {
            // Get number of living neighbors
            int neighbors = get_alive_neighbors(matrix, r, c);

            // Get current state
            int current_state = get_cell_state(matrix, r, c);

            // The cell is alive
            if (current_state > 0)
            {
                set_cell_state(&new_matrix, r, c, matrix->survive[neighbors]);
            }
            // The cell is dead
            else
            {
                set_cell_state(&new_matrix, r, c, matrix->born[neighbors]);
            }
        }
    }

    for (int r = 0; r < matrix->rows; r++)
    {
        for (int c = 0; c < matrix->cols; c++)
        {
            set_cell_state(matrix, r, c, get_cell_state(&new_matrix, r, c));
        }
    }
}
