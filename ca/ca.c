/******************************************************************************
BSD 3-Clause License

Copyright (c) 2019, Valerio Orlandini
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/

#define MAXAPI_USE_MSCRT

#include "ext.h"
#include "ext_obex.h"

#include "cellauto.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct _ca
{
    t_object s_obj;     // t_object header
    ca_matrix matrix;
    void *outlet;
} t_ca;

void *ca_new();
void ca_bang(t_ca *x);
void ca_fill(t_ca *x);
void ca_born(t_ca *x, t_symbol *s, long argc, t_atom *argv);
void ca_surv(t_ca *x, t_symbol *s, long argc, t_atom *argv);
void ca_cols(t_ca *x, long cols);
void ca_rows(t_ca *x, long rows);
void ca_muta(t_ca *x, long cells);
void ca_cell(t_ca *x, t_symbol *s, long argc, t_atom *argv);

static t_class *ca_class; // global pointer to our class definition that is setup in ext_main()

void ext_main(void *r)
{
    t_class *c;
    srand(time(NULL));
    c = class_new("CA", (method)ca_new, (method)NULL, sizeof(t_ca), 0L, 0);
    class_addmethod(c, (method)ca_bang, "bang", 0);
    class_addmethod(c, (method)ca_born, "born", A_GIMME, 0);
    class_addmethod(c, (method)ca_surv, "surv", A_GIMME, 0);
    class_addmethod(c, (method)ca_cols, "cols", A_LONG, 0);
    class_addmethod(c, (method)ca_rows, "rows", A_LONG, 0);
    class_addmethod(c, (method)ca_fill, "fill", 0);
    class_addmethod(c, (method)ca_cell, "cell", A_GIMME, 0);
    class_addmethod(c, (method)ca_muta, "muta", A_LONG, 0);
    class_register(CLASS_BOX, c);
    ca_class = c;
}

void *ca_new()
{
    t_ca *x = (t_ca *)object_alloc(ca_class);

    init_matrix(&(x->matrix), 4, 8);

    x->outlet = outlet_new((t_object *)x, NULL);

    return x;
}

void ca_bang(t_ca *x)
{
    update_matrix(&(x->matrix));

    t_atom argv[(3 * (x->matrix.rows * x->matrix.cols))];

    int arg = 0;

    for (int r = 0; r < x->matrix.rows; r++)
    {
        for (int c = 0; c < x->matrix.cols; c++)
        {
            atom_setlong(argv + arg++, c);
            atom_setlong(argv + arg++, r);
            atom_setlong(argv + arg++, get_cell_state(&(x->matrix), r, c));
        }
    }

    outlet_anything(x->outlet, gensym("set"), (3 * (x->matrix.rows * x->matrix.cols)), argv);
}

void ca_born(t_ca *x, t_symbol *s, long argc, t_atom *argv)
{
    long i;
    t_atom *ap;

    for (int n = 0; n < 9; n++)
    {
        set_rule(&(x->matrix), 0, n, 0);
    }

    for (i = 0, ap = argv; i < argc; i++, ap++)
    {
        switch (atom_gettype(ap))
        {
        case A_LONG:
            if (atom_getlong(ap) < 9)
            {
                set_rule(&(x->matrix), 0, atom_getlong(ap), 1);
            }
            break;
        default:
            post("%ld: unknown atom type (%ld)", i+1, atom_gettype(ap));
            break;
        }
    }
}

void ca_surv(t_ca *x, t_symbol *s, long argc, t_atom *argv)
{
    long i;
    t_atom *ap;

    for (int n = 0; n < 9; n++)
    {
        set_rule(&(x->matrix), 1, n, 0);
    }

    for (i = 0, ap = argv; i < argc; i++, ap++)
    {
        switch (atom_gettype(ap))
        {
        case A_LONG:
            if (atom_getlong(ap) < 9)
            {
                set_rule(&(x->matrix), 1, atom_getlong(ap), 1);
            }
            break;
        default:
            post("%ld: unknown atom type (%ld)", i+1, atom_gettype(ap));
            break;
        }
    }
}

void ca_cols(t_ca *x, long cols)
{
    if (cols >= 1)
    {
        resize_matrix(&(x->matrix), x->matrix.rows, cols);
    }
}

void ca_rows(t_ca *x, long rows)
{
    if (rows >= 1)
    {
        resize_matrix(&(x->matrix), rows, x->matrix.cols);
    }
}

void ca_fill(t_ca *x)
{
    for (int r = 0; r < x->matrix.rows; r++)
    {
        for (int c = 0; c < x->matrix.cols; c++)
        {
            set_cell_state(&(x->matrix), r, c, rand() % 2);
        }
    }

    t_atom argv[(3 * (x->matrix.rows * x->matrix.cols))];

    int arg = 0;

    for (int r = 0; r < x->matrix.rows; r++)
    {
        for (int c = 0; c < x->matrix.cols; c++)
        {
            atom_setlong(argv + arg++, c);
            atom_setlong(argv + arg++, r);
            atom_setlong(argv + arg++, get_cell_state(&(x->matrix), r, c));
        }
    }

    outlet_anything(x->outlet, gensym("set"), (3 * (x->matrix.rows * x->matrix.cols)), argv);
}

void ca_muta(t_ca *x, long cells)
{
    if (cells == 0)
    {
        cells = 1;
    }

    for (int c = 0; c < cells; c++)
    {
        int row = rand() % x->matrix.rows;
        int col = rand() % x->matrix.cols;
        int state = 1;

        if (get_cell_state(&(x->matrix), row, col) > 0)
        {
            state = 0;
        }

        set_cell_state(&(x->matrix), row, col, state);
        //post("Row %ld, Col %ld, State %ld\n", row, col, state);
    }
}

void ca_cell(t_ca *x, t_symbol *s, long argc, t_atom *argv)
{
    long c;
    int coords[3] = {0, 0, 0};

    for (c = 0; c < 3; c++)
    {
        if (c >= argc)
        {
            return;
        }
        switch (atom_gettype(argv + c))
        {
        case A_LONG:
            coords[c] = atom_getlong(argv + c);
            break;
        default:
            post("%ld: unknown atom type (%ld)", c + 1, atom_gettype(argv));
            break;
        }
    }

    set_cell_state(&(x->matrix), coords[1], coords[0], coords[2]);

    t_atom argv_s[3];

    atom_setlong(argv_s, coords[0]);
    atom_setlong(argv_s + 1, coords[1]);
    atom_setlong(argv_s + 2, get_cell_state(&(x->matrix), coords[1], coords[0]));

    outlet_anything(x->outlet, gensym("set"), 3, argv_s);
}
